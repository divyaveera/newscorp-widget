# newscorp-widget

install maven 3
 
install java
 
ensure M2_HOME and JAVA_HOME are set in environment variable and PATH updated with JAVA_HOME/bin and M2_HOME/bin
 
git clone https://divyaveera@bitbucket.org/divyaveera/newscorp-widget.git
 
mvn package
 
mvn exec:java -Dexec.mainClass="au.com.news.widget.WidgetImporter"
 
you will be prompted to enter the json file path
<file path>
 
you will be prompted to enter property device or site
<device/site>
 
you will be prompted to enter search keyword
<mobile> if device entered as property
 
type quit and hit enter to exit the program, else you will be prompted to search again
 
to run unit tests only, type
mvn test