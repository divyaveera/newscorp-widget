package au.com.news.widget;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import com.fasterxml.jackson.databind.ObjectMapper;

import au.com.news.widget.model.Widget;

public class FileParser {

    public static Widget getWidget(File file) throws Exception {
        BufferedReader br = new BufferedReader(new FileReader(file));
        String st = "";
        StringBuffer sb = new StringBuffer();
        while ((st = br.readLine()) != null) {
        	//replace whitespace in keys like 'image gallery'
            sb.append(st.replaceAll("\\s",""));
        }
        br.close();
        ObjectMapper mapper = new ObjectMapper();
        Widget obj = mapper.readValue(sb.toString(), Widget.class);
        return obj;
    	//return null;
    }
    
}