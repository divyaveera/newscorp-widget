package au.com.news.widget.model;

public class Widget {

    private Imagegallery imagegallery;

    private Query query;

    private Newsstory newsstory;

    private Ad ad;

    private Video video;

    public Imagegallery getImagegallery ()
    {
        return imagegallery;
    }

    public void setImagegallery (Imagegallery imagegallery)
    {
        this.imagegallery = imagegallery;
    }

    public Query getQuery ()
    {
        return query;
    }

    public void setQuery (Query query)
    {
        this.query = query;
    }

    public Newsstory getNewsstory ()
    {
        return newsstory;
    }

    public void setNewsstory (Newsstory newsstory)
    {
        this.newsstory = newsstory;
    }

    public Ad getAd ()
    {
        return ad;
    }

    public void setAd (Ad ad)
    {
        this.ad = ad;
    }

    public Video getVideo ()
    {
        return video;
    }

    public void setVideo (Video video)
    {
        this.video = video;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [imagegallery = "+imagegallery+", query = "+query+", newsstory = "+newsstory+", ad = "+ad+", video = "+video+"]";
    }

}
