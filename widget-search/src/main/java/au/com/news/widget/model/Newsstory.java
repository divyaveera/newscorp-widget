package au.com.news.widget.model;

public class Newsstory
{
    private String site;

    private String device;

    public String getSite ()
    {
        return site;
    }

    public void setSite (String site)
    {
        this.site = site;
    }

    public String getDevice ()
    {
        return device;
    }

    public void setDevice (String device)
    {
        this.device = device;
    }

    @Override
    public String toString()
    {
        return "Newsstory [site = "+site+", device = "+device+"]";
    }
}