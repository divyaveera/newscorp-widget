package au.com.news.widget;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import au.com.news.widget.model.Widget;

public class WidgetHelper {

    public static List<String> deviceList = new ArrayList<String>();

    public static List<String> siteList = new ArrayList<String>();

    static {
        deviceList.add("mobile");
        deviceList.add("desktop");
        deviceList.add("all");
        siteList.add("perthnow");
        siteList.add("dailytelegraph");
        siteList.add("theaustralian");
        siteList.add("all");
    }
	
	public static Widget getWidget() {
		Widget widget = null;
		while(true) {
            System.out.println("Enter widget data file location...");
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                String filePath = br.readLine();
                if("quit".equalsIgnoreCase(filePath)) {
                    System.out.println("You chose to exit the program. Thank you!");
                    System.exit(0);
                }
                widget = getWidgetFromFile(filePath);
            } catch(Exception e) {
                System.out.println("Could not read this file. File missing or data corrupted... Type quit anytime to abort");
                continue;
            }
            break;
        }
		return widget;
	}
	
	public static Widget getWidgetFromFile(String filePath) throws Exception {
		File widgetDataFile = new File(filePath);
        return FileParser.getWidget(widgetDataFile);
	}
	
	public static String getProperty() {
		String property = "";
		while(true) {
            System.out.println("Property: Enter device or site and enter... Type quit anytime to abort");
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                property = br.readLine();
                if("quit".equalsIgnoreCase(property)) {
                    System.out.println("You chose to exit the program. Thank you!");
                    System.exit(0);
                }
                if(!"device".equalsIgnoreCase(property) && !"site".equalsIgnoreCase(property)) {
                    System.err.println("Enter a valid property device or site... Type quit anytime to abort");
                    continue;
                }
            } catch(Exception e) {
                System.err.println("Enter a valid property device or site... Type quit anytime to abort");
                continue;
            }
            break;
        }
		return property;
	}
	
	public static String getSearchKeyword(String property) {
		String searchKeyword = "";
        while(true) {
            System.out.println("Now enter search keyword for " + property + "... Type quit anytime to abort");
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                searchKeyword = br.readLine();
                if("quit".equalsIgnoreCase(searchKeyword)) {
                    System.out.println("You chose to exit the program. Thank you!...");
                    System.exit(0);
                }
                if(property.equalsIgnoreCase("device")) {
                    if(!deviceList.contains(searchKeyword)) {
                        System.err.println("Invalid search keyword for device");
                        continue;
                    }
                } else if(property.equalsIgnoreCase("site")) {
                    if(!siteList.contains(searchKeyword)) {
                        System.err.println("Invalid search keyword for site");
                        continue;
                    }
                }
            } catch(Exception e) {
                System.err.println(e.getMessage());
                continue;
            }
            break;
        }
        return searchKeyword;
	}

}