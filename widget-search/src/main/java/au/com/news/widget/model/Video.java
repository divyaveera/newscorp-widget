package au.com.news.widget.model;

public class Video
{
    private String[] site;

    private String device;

    public String[] getSite ()
    {
        return site;
    }

    public void setSite (String[] site)
    {
        this.site = site;
    }

    public String getDevice ()
    {
        return device;
    }

    public void setDevice (String device)
    {
        this.device = device;
    }

    @Override
    public String toString()
    {
        return "Video [site = "+site+", device = "+device+"]";
    }
}