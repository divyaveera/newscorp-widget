package au.com.news.widget;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import au.com.news.widget.model.Widget;

public class WidgetImporter {
	
	public static List<String> getSearchResults(Widget widget, String property, String searchKeyword) {
		List<String> searchResult = new ArrayList<String>();
		if(property.equalsIgnoreCase("device")) {
        	if(searchKeyword.equalsIgnoreCase(widget.getAd().getDevice()) || "all".equalsIgnoreCase(widget.getAd().getDevice())) {
        		searchResult.add("ad");
        	}
        	if(searchKeyword.equalsIgnoreCase(widget.getImagegallery().getDevice()) || "all".equalsIgnoreCase(widget.getImagegallery().getDevice())) {
        		searchResult.add("image gallery");
        	}
        	if(searchKeyword.equalsIgnoreCase(widget.getNewsstory().getDevice()) || "all".equalsIgnoreCase(widget.getNewsstory().getDevice())) {
        		searchResult.add("news story");
        	}
        	if(searchKeyword.equalsIgnoreCase(widget.getQuery().getDevice()) || "all".equalsIgnoreCase(widget.getQuery().getDevice())) {
        		searchResult.add("query");
        	}
        	if(searchKeyword.equalsIgnoreCase(widget.getVideo().getDevice()) || "all".equalsIgnoreCase(widget.getVideo().getDevice())) {
        		searchResult.add("video");
        	}
        } else if(property.equalsIgnoreCase("site")) {
        	if(searchKeyword.equalsIgnoreCase(widget.getAd().getSite()) || "all".equalsIgnoreCase(widget.getAd().getSite())) {
        		searchResult.add("ad");
        	}
        	if(Arrays.asList(widget.getImagegallery().getSite()).contains(searchKeyword)) searchResult.add("image gallery");
        	if(searchKeyword.equalsIgnoreCase(widget.getNewsstory().getSite()) || "all".equalsIgnoreCase(widget.getNewsstory().getSite())) {
        		searchResult.add("news story");
        	}
        	if(Arrays.asList(widget.getQuery().getSite()).contains(searchKeyword)) searchResult.add("query");
        	if(Arrays.asList(widget.getVideo().getSite()).contains(searchKeyword)) searchResult.add("video");
        }
		return searchResult;
	}

    public static void main(String[] args) {
    	System.out.println("current dir: " + System.getProperty("user.dir"));
        System.out.println("Type quit and hit enter to exit searching");
        
        //Get data file as input and convert that to Widget model
        Widget widget = WidgetHelper.getWidget();
        
        
        while(true) {
        	//Get property from command line device or site
            String property = WidgetHelper.getProperty();
            
            //Get search keyword from command line
            String searchKeyword = WidgetHelper.getSearchKeyword(property);
            
            //compute search results from widget for given property and keyword
            List<String> searchResult = getSearchResults(widget, property, searchKeyword);

			System.out.println("----------------------------------------------------------------------");
            System.out.println("Search results: " + searchResult);
            System.out.println("----------------------------------------------------------------------");
        }
        
        
        

    }

}