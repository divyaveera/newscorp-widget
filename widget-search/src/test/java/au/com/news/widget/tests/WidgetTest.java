package au.com.news.widget.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.util.List;

import org.junit.Test;

import au.com.news.widget.WidgetHelper;
import au.com.news.widget.WidgetImporter;
import au.com.news.widget.model.Widget;

public class WidgetTest {

	@Test
	public void testDevice() {
		try {
			Widget widget = WidgetHelper.getWidgetFromFile(System.getProperty("user.dir") + File.separator + 
					"src" + File.separator + "test" + File.separator + "resources" + File.separator + "widget.json");
			List<String> searchResult = WidgetImporter.getSearchResults(widget, "device", "mobile");
			assertTrue(searchResult.contains("ad"));
			assertTrue(searchResult.size() == 3);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void testSite() {
		try {
			Widget widget = WidgetHelper.getWidgetFromFile(System.getProperty("user.dir") + File.separator + 
					"src" + File.separator + "test" + File.separator + "resources" + File.separator + "widget.json");
			List<String> searchResult = WidgetImporter.getSearchResults(widget, "site", "perthnow");
			assertTrue(searchResult.contains("image gallery"));
			assertTrue(searchResult.size() == 4);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
